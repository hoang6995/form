"use strict";
function getLength(number){
    return number.toString().length;
}
function calculateAge(birthday) {
    var today = new Date();
    var birthDate = new Date(birthday);
    var yourAge = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        yourAge--;
    }
    return yourAge;
}

function validateForm(){

    var errMsg = "";
    var result = true;
    
    var number = document.getElementById("Phone").value.trim();
    var DOB = document.getElementById("DOB").value.trim();
    var yourAge = calculateAge(DOB);
    
    
    if(getLength(number) < 1){
        errMsg += "Please enter country code !";
    }
    if(yourAge < 13){
        errMsg += "You are under 13 years old";
    }
    if(yourAge > 50){
        errMsg += "You are over 50 years old";
    }
    if(errMsg!=""){
        alert(errMsg);
        result = false;
    }
    
    return result;
}
function init(){
    if(document.getElementById("form")!=null){
        document.getElementById("form").onsubmit = validateForm;
    }
}
window.onload = init;